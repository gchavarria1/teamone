import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author eryn
 *
 */
public class MainClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String firstName = JOptionPane.showInputDialog("Please enter your First Name.");
		String lastName = JOptionPane.showInputDialog("Please enter your Last Name.");
		String[] cuisineArray = { "Italian", "American", "TexMex", "French", "Mediteranian", "Chinese", "Japanese" };
		String cuisine = (String)JOptionPane.showInputDialog( null, "Please choose your favorite cuisine.", "Cuisine ...",
	            JOptionPane.QUESTION_MESSAGE, 
	            null, 
	            cuisineArray,
	            cuisineArray[ 0 ] );
		String Restaurant = JOptionPane.showInputDialog("What is your favorite restaurant?");
		String[] carryOut = { "Dine-In", "Carry-Out", "Drive-Thru", "Delivery" };
		String service = (String)JOptionPane.showInputDialog( null, "Which do you prefer?", "Style of Service ...",
	            JOptionPane.QUESTION_MESSAGE, 
	            null, 
	            carryOut,
	            carryOut[ 0 ] );
			
		String[] bar = { "I prefer a bar", "I do not prefer a bar" };
		String Bar = (String)JOptionPane.showInputDialog( null, "Do you prefer bar service?", "Bar Preference ...",
	            JOptionPane.QUESTION_MESSAGE, 
	            null, 
	            bar,
	            bar[ 0 ] );
		
			
			System.out.println("Please verify your information:");
			System.out.println("Name: " + firstName + lastName);
			System.out.println("Favorite Cuisine: " + cuisine);
			System.out.println("Favorite Restaurant: " + Restaurant);
			System.out.println("Prefered style of service: " + service);
			System.out.println("Bar service preference: " + Bar);
	}
		}