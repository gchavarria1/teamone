import javax.swing.JOptionPane;
/**
 * 
 */

/**
 * @author Gabriel Chavarria
 *
 */
public class WelcomePage {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	//pushing for test
		
	JOptionPane.showMessageDialog(null, "Welcome to Hangry Match!");
	
	
	CHistory();
	ATeam();
	CMission();
	ContactUs();
		
				
	}
	public static void CHistory() 
	{
		JOptionPane.showMessageDialog(null, "Company History: " + "\n" 
	+ "\n" + "Our company was founded and established on January 28, 2021." +
		" Each member has a unique" + "\n"+ "background but we all shared the same mission"
	+ " when we collaborated. To serve our community.");
		return;
		
	}
	
	public static void ATeam()
	{
		JOptionPane.showMessageDialog(null, "Meet the team: " + "\n" +
				"Gabriel Chavarria" + "\n" + "Eryn Edwards" + "\n" + "Kennedy Blackman"
				+"\n" + "Vida Bezejouh" + "\n" + "Kelly Arzola");
		return;
				
	}
	public static void CMission()
	{
		JOptionPane.showMessageDialog(null, "Company Mission: " + "\n" + "\n" +
		"To create an application that helps couples/friends match with their "
		+ "community on food choices" + "\n" + "in a world of possibilities. Each day is "
		+ "different from the next so as time changes so does our food mood.");
		return;
	}
	public static void ContactUs()
	{
		JOptionPane.showMessageDialog(null, "For any questions or concerns, please"+ 
	" contact us: "+ "\n" + "Gabriel Chavarria - gchavarria@hangry.com" + "\n" +
	"Eryn Edwards - eedwards@hangry.com" + "\n" + "Kennedy Blackman - kblackman@hangry.com"
	+ "\n" + "Vida Bezejouh - vbezejouh@hangry.com" + "\n" + "Kelly Arzola - karzoal @hangry.com");
		return;
		
	}
			      
			    	  
	}


